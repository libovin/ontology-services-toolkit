/*******************************************************************************
 * Copyright 2017 by the Department of Informatics (University of Oslo)
 * 
 *    This file is part of the Ontology Services Toolkit 
 *
 *******************************************************************************/
package no.ifi.uio.ontology_services_toolkit;

import java.io.FileNotFoundException;
import java.util.TreeSet;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import uio.ifi.ontology.toolkit.projection.controller.reasoner.ReasonerManager.OWL2Reasoner;
import uio.ifi.ontology.toolkit.projection.controller.triplestore.RDFoxProjectionManagerForEmbeddings;
import uio.ifi.ontology.toolkit.projection.controller.triplestore.RDFoxSessionManager;
import uio.ifi.ontology.toolkit.projection.model.entities.Concept;
import uio.ifi.ontology.toolkit.projection.view.OptiqueVQSAPI;


/**
 *
 * @author ernesto
 * Created on 14 Nov 2017
 *
 */
public class TestProjection {
	
	public static void main (String args[]){
		
		
		try {
			
			
			RDFoxSessionManager session_rdf = new RDFoxSessionManager();
			
			//String onto_file = "file:/home/ejimenez-ruiz/Documents/ontologies/go.owl";
			//String onto_projection = "file:/home/ejimenez-ruiz/Documents/ontologies/go_projection.ttl";
			
			
			//String onto_file = "file:/home/ernesto/Documents/Datasets/Food/foodon-merged.train.owl";
			//String onto_projection = "/home/ernesto/Documents/Datasets/Food/foodon-merged.train.projection.ttl";
			
			
			String onto_file = "file:/home/ernesto/Documents/Datasets/Food/helis_v1.00.train.owl";
			String onto_projection = "/home/ernesto/Documents/Datasets/Food/helis_v1.00.train.projection.ttl";
			
			
			
			//TODO OWL 2 classification with HermiT can now be switched off
			OWL2Reasoner owl2classification = OWL2Reasoner.ELK;
			RDFoxProjectionManagerForEmbeddings projection = new RDFoxProjectionManagerForEmbeddings(onto_file, owl2classification);
			projection.exportMaterielizationSnapshot(onto_projection);

			
			
			//session_rdf.createNewSessionForEmbeddings(onto_file);
			
			//session_rdf.getSession(onto_file).exportMaterielizationSnapshot(onto_projection);
			
			
			if (true)
				return;
			
			
			
			String onto_url = "file:/home/ernesto/Desktop/example.owl";
			
			OWLOntologyManager manager = OWLManager.createConcurrentOWLOntologyManager();
			
			//new RDFoxProjectionManager(manager.loadOntology(IRI.create(onto_url)), true, true);	
			
			RDFoxSessionManager session = new RDFoxSessionManager();
			
			OptiqueVQSAPI vqs = new OptiqueVQSAPI(session);
			
			vqs.loadOntologySession(onto_url);
			
			System.out.println("Loaded ontos: \n "+ vqs.getOntologies());
			
			
			
			//System.out.println("Core: \n "+ vqs.getCoreConcepts(onto_url).toString(1));
			System.out.println("Core: \n "+ vqs.getCoreConcepts(onto_url).toString(1));
			
			
			TreeSet<Concept> concepts = vqs.getSessionManager().getSession(onto_url).getCoreConcepts();
			
			for (Concept c: concepts){
				System.out.println("Links for " + c.getName() + ": \n "+ vqs.getNeighbourConcepts(onto_url, c.getIri()).toString(1));
			}
			
			
			
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

}
