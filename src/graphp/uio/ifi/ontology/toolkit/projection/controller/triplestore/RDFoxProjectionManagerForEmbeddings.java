package uio.ifi.ontology.toolkit.projection.controller.triplestore;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import uio.ifi.ontology.toolkit.constraint.utils.Utility;
import uio.ifi.ontology.toolkit.constraint.utils.pagoda_hermit.Timer;
import uio.ifi.ontology.toolkit.projection.controller.reasoner.ReasonerManager.OWL2Reasoner;
import uk.ac.ox.cs.JRDFox.JRDFoxException;
import uk.ac.ox.cs.JRDFox.store.DataStore;

public class RDFoxProjectionManagerForEmbeddings extends RDFoxProjectionManager{

	
	
	public RDFoxProjectionManagerForEmbeddings(OWLOntology ontology, OWL2Reasoner reasonerID) throws JRDFoxException, IOException {
		super(ontology, "", false, false, reasonerID, false); //no propagation and no facets

	}
	
	
	public RDFoxProjectionManagerForEmbeddings(String ontology_iri, OWL2Reasoner reasonerID) throws JRDFoxException, OWLOntologyCreationException, IOException {
		super(ontology_iri, "", false, false, reasonerID, false); //no propagation and no facets
	}
	
	
	
	public RDFoxProjectionManagerForEmbeddings(Set<String> ontology_iris, OWL2Reasoner reasonerID) throws JRDFoxException, OWLOntologyCreationException, IOException {
		super(ontology_iris, "", false, false, reasonerID, false); //no propagation and no facets
	}
	
	
	
	
	protected void perfromMaterialization() throws JRDFoxException{
		
		//store = new DataStore(DataStore.StoreType.ParallelSimpleNN, EqualityAxiomatizationType.Off);
		store = new DataStore(DataStore.StoreType.ParallelSimpleNN);
		
		try{			
			
			store.setNumberOfThreads(8);
			
			//1. Import rules from classification and inverse axioms
			Timer t = new Timer();
			
			//We import the classified ontology rules (inverses are the required ones specially) and also the
			//projection of the classification (see GraphProjectionManager)
			//TODO Not necessary for embeddings
			//Utility.println("Importing classified ontology...");
			//store.importOntology(getClassifiedOntology());
						
			//2. Import projection data
			Utility.println("Importing RDF data (projection)...");
			store.importFiles(new File[] {new File(tmp_file_projection)});//, new File(tmp_file)});
			number_initial_triples = store.getTriplesCount();
			Utility.println("Number of tuples after data import: " + store.getTriplesCount());
			
			
			
			
			StringBuilder rule_builder = new StringBuilder(); 
			
			//3a. Import equality rules			
			Utility.println("Importing rules...");			
			//File equality_file = new File(Constants.working_directory + "equality.dlog");
			//store.importFiles(new File[] {equality_file});
			getTextForPrefixes(rule_builder);
			
			//TODO DO we need them here?
			getTextForEqualityRules(rule_builder);
			
			//
			
			
			//3b. Merge Domain and Range axiom triples
			getTextForRangeDomainPropagationRules(rule_builder);
			
			//4. Add rules to saturate graph: top-bottom
			if (topBottomPropagation)
				getTextForTopBottomPropagationRules(rule_builder);
			
			store.importText(rule_builder.toString());			
			Utility.println("Importing time RDFox: " + t.durationMilisecons()  + " (ms)");
			
			
			//5. Materialization
			Utility.println("Standard materialization + Top-down propagation...");
			t = new Timer();
			store.applyReasoning();
			//Utility.println("Materialization time RDFox: " + t.duration()  + " (s)");
			axiom_materialization_time = t.durationMilisecons();
			Utility.println("Materialization time RDFox 1: " + axiom_materialization_time  + " (ms)");

			number_triples_materialization_axioms = store.getTriplesCount();
			Utility.println("Number of tuples after materialization 1: " + store.getTriplesCount());
			
			
			
			//6. Make facts explicit
			//Necessary to avoid the application of other rules like bottom-up 
			store.clearRulesAndMakeFactsExplicit();//Important to clear rules
			//store.makeFactsExplicit();
			
			
			
				
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
	}

	
	
	
}
